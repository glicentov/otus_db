﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace OTUS_DB
{
    class Program
    {
        // Для рандомизации данных, там где это возможно, использовал вариант с получением значения из Random
        static void Main(string[] args)
        {
            SetUsersTable();
            SetOrdersTable();
            SetGoodsTable();
            ShowDataFromTable();
            CreateNewRecordInUserTable();
        } 
        private static void SetUsersTable()
        {
            List<User> UsersList = new List<User>(); // В этом List'е храним объекты для вставки в таблицу Users
            var PhoneNumRandGenerator = new Random();  // генератор номеров телефонов
            UsersList.Add(new User("Paul", "Smith", PhoneNumRandGenerator.Next(700000, 800000)));
            UsersList.Add(new User("Frank", "Bonstrem", PhoneNumRandGenerator.Next(700000, 800000)));
            UsersList.Add(new User("Boby", "Cannahem", PhoneNumRandGenerator.Next(700000, 800000)));
            UsersList.Add(new User("Renner", "Boenmenner", PhoneNumRandGenerator.Next(700000, 800000)));
            UsersList.Add(new User("Cristopher", "Shultz", PhoneNumRandGenerator.Next(700000, 800000)));

            using (ApplicationContext applContext = new ApplicationContext())
            {
                for (int i = 0; i < UsersList.Count; i++) // Пробегаемся по всем инстансам 
                {
                    applContext.Users.Add(UsersList[i]); // Отправляем инстансы в БД
                }
                var cntRec = applContext.SaveChanges();
                Console.WriteLine($"The {cntRec} records were successfully inserted to the Users table");
            }
        }
        private static void SetOrdersTable()
        {
            List<Order> OrdersList = new List<Order>(); // В этом List'е храним объекты для вставки в таблицу Orders
            // (Today - Random Days) - используется для генерации рандомного значения даты ордеров 
            var TimeSpanRandGenerator = new Random(); // Генератор количества дней для формирования даты заказа

            OrdersList.Add(new Order(1, DateTime.Today - new TimeSpan(TimeSpanRandGenerator.Next(1,50), 2, 2, 1),userId: 1));
            OrdersList.Add(new Order(2, DateTime.Today - new TimeSpan(TimeSpanRandGenerator.Next(1,50), 2, 2, 1),userId: 1));
            OrdersList.Add(new Order(3, DateTime.Today - new TimeSpan(TimeSpanRandGenerator.Next(1,50), 2, 2, 1),userId: 3));
            OrdersList.Add(new Order(4, DateTime.Today - new TimeSpan(TimeSpanRandGenerator.Next(1,50), 2, 2, 1),userId: 5));
            OrdersList.Add(new Order(5, DateTime.Today - new TimeSpan(TimeSpanRandGenerator.Next(1,50), 2, 2, 1),userId: 5));

            using (ApplicationContext applContext = new ApplicationContext())
            {
                for (int i = 0; i < OrdersList.Count; i++)
                {
                    applContext.Orders.Add(OrdersList[i]);
                }
                var cntRec = applContext.SaveChanges();
                Console.WriteLine($"The {cntRec} records were successfully inserted to the Order table");
            }
        }
        private static void SetGoodsTable()
        {
            List<Item> ItemList = new List<Item>(); // В этом List'е храним объекты для вставки в таблицу Orders
            // (Today - Random Days) - используется для генерации рандомного значения даты ордеров 
            var ItemNumRandGenerator = new Random(); // Генератор номера номенклатуры

            ItemList.Add(new Item(ItemNumRandGenerator.Next(0, 500), "Car", 1)) ;
            ItemList.Add(new Item(ItemNumRandGenerator.Next(0, 500), "Pen", 2));
            ItemList.Add(new Item(ItemNumRandGenerator.Next(0, 500), "Window", 3));
            ItemList.Add(new Item(ItemNumRandGenerator.Next(0, 500), "Chair", 4));
            ItemList.Add(new Item(ItemNumRandGenerator.Next(0, 500), "Toy", 5));

            using (ApplicationContext applContext = new ApplicationContext())
            {
                for (int i = 0; i < ItemList.Count; i++)
                {
                    applContext.Items.Add(ItemList[i]); // Пробегаемся по всем инстансам и отправляем их в БД
                }
                var cntRec = applContext.SaveChanges();
                Console.WriteLine($"The {cntRec} records were successfully inserted to the Goods table");
            }
        }

        private static void ShowDataFromTable()
        {
            using (ApplicationContext applContext =  new ApplicationContext() )
            {
                // Вывод всех записей из Users
                var users = applContext.Users.ToList();
                foreach (var user in users)
                {
                    Console.WriteLine($"UserID: { user.UserId}\nUserName: {user.Name}\n ===");
                }
                var orders = applContext.Orders.ToList();
                foreach (var order in orders)
                {
                    Console.WriteLine($"OrderNum: { order.OrderNum}\nOrderDate: {order.OrderDate}\n ===");
                }
                var items = applContext.Items.ToList();
                foreach (var item in items)
                {
                    Console.WriteLine($"ItemId: {item.ItemId }\nItemName: {item.ItemName}\n ===");
                }

            }
            
        }
        private static void CreateNewRecordInUserTable()
        {
            Console.WriteLine("You can add the new line to the Users table");
            Console.WriteLine("If you want please input new string with tabspace");
            Console.WriteLine("in format - [Name] [SurName]");
            Console.WriteLine("Id and PhoneNumeb will be generated automatically");
            Console.WriteLine("If you don't want to input - please ented end");
            string inputString = default;
            while (inputString != "end")
            {
                var proformaUser = Console.ReadLine().Split(' '); // разбиваем строку на имя и фамилию
                if (proformaUser[0].ToLower() == "end")
                {
                    inputString = "end";
                    break;
                }
                else
                {
                    using (ApplicationContext applContext = new ApplicationContext())
                    {
                        var newUser = new User(name: proformaUser[0].ToUpperInvariant(),
                                            surName: proformaUser[1].ToUpperInvariant(),
                                            phoneNum: new Random().Next(700000, 800000));
                        applContext.Users.Add(newUser);
                        var cntRec = applContext.SaveChanges();
                        Console.WriteLine($"The {cntRec} records were successfully inserted to the Users table");
                        var users = applContext.Users.ToList();
                        foreach (var user in users)
                        {
                            Console.WriteLine($"UserID: { user.UserId}\nUserName: {user.Name}\n ===");
                        }
                    }
                }
            }
        }
    }
        
}
