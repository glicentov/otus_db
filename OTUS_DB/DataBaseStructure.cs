﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace OTUS_DB
{
    public class User // Таблицы пользователей
    {
        [Key]
        public int UserId { get; set; } // Идентификатор пользователя
        public string Name { get; set; } // Имя
        public string SurName { get; set; } // Фамилия
        public long PhoneNum { get; set; } // Телефонный номер

        public User(string name, string surName, long phoneNum)
        {
            Name = name;
            SurName = surName;
            PhoneNum = phoneNum;
        }

    }
    public class Order // Таблицы заказов
    {
        [Key]
        public int Id { get; set; }
        public int OrderNum { get; set; } // номер заказа
        public DateTime OrderDate { get; set; } // дата заказа
        public int UserId { get; set; }
        [ForeignKey("UserId")] // внешний ключ явно устанавливаем на поле Users.UserId
        public User Users { get; set; }  // ссылка на таблицу Users 

        public Order(int orderNum, DateTime orderDate, int userId)
        {
            OrderNum = orderNum;
            OrderDate = orderDate;
            UserId = userId;
        }
    }

    public class Item
    {
        [Key]
        public int Id { get; set; }
        public int ItemId { get; set; } // Идентификатор товара
        public string ItemName { get; set; }// Наименование товара
        public int OrderNum { get; set; }
        [ForeignKey("OrderNum")] // Внешний ключ явно устанавливаем на поле Orders.OrderNum 
        public Order Orders { get; set; } // Ссылка на таблицу заказов

        public Item(int itemId, string itemName, int orderNum)
        {
            ItemId = itemId;
            ItemName = itemName;
            OrderNum = orderNum;
        }
    }
    
}
